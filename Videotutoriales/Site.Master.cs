﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Videotutoriales
{
  public partial class SiteMaster : MasterPage
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Log_out(object sender, EventArgs e)
    {
      Session.Clear();
      Session.Abandon();
      FormsAuthentication.SignOut();
      FormsAuthentication.RedirectToLoginPage();
      Response.Redirect("Login");
    }
  }
}