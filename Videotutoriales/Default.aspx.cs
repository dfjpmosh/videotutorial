﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Videotutoriales.App_Code;

namespace Videotutoriales
{
  public partial class Default : System.Web.UI.Page
  {
    private static string dirPrincipal = "Videos";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string ListaVideos2(string carpeta)
    {
      return JsonConvert.SerializeObject(listarVideos(carpeta));
    }

    public static DataTable listarVideos(string carpeta)
    {
      DataTable dtVideos = new DataTable();
      
      dtVideos.Columns.Add("Nombre");
      dtVideos.Columns.Add("Link");

      System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(dirPrincipal + @"\" + carpeta.Replace("#", @"\")));
      foreach (System.IO.FileInfo file in dir.GetFiles("*.*"))
      {
        DataRow row = dtVideos.NewRow();
        row["Nombre"] = file.Name.Replace(file.Extension, "");
        row["Link"] = dirPrincipal + carpeta.Replace("#", @"\") + @"\" + file.Name;

        dtVideos.Rows.InsertAt(row, 0);
      }

      return dtVideos;
    }

    [WebMethod]
    public static string Menu(int K_Usuario)
    {
      return JsonConvert.SerializeObject(listarDirectorios(K_Usuario));
    }
    public static DataTable listarDirectorios(int K_Usuario)
    {
      int cont = 0, contPadre;
      DataTable dtDirectorios = new DataTable();
      DataTable dtCarpetasUsuario = ClassSeguridad.SK_Carpetas_Usuarios(K_Usuario);

      dtDirectorios.Columns.Add("K_Opcion");
      dtDirectorios.Columns.Add("K_Opcion_Padre");
      dtDirectorios.Columns.Add("D_Opcion");
      dtDirectorios.Columns.Add("Enlace");
      dtDirectorios.Columns.Add("Icon");
      dtDirectorios.Columns.Add("Oculto");

      System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(dirPrincipal + @"\"));
      foreach (System.IO.DirectoryInfo d in dir.GetDirectories())
      {
        cont++;

        DataRow row = dtDirectorios.NewRow();
        row["K_Opcion"] = cont;
        row["K_Opcion_Padre"] = 0;
        row["D_Opcion"] = d.Name;
        row["Enlace"] = "";
        row["Icon"] = "far fa-folder";
        row["Oculto"] = false;

        dtDirectorios.Rows.InsertAt(row, 0);

        System.IO.DirectoryInfo dirhijo = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(dirPrincipal + @"\" + d.Name));
        contPadre = cont;
        foreach (System.IO.DirectoryInfo dh in dirhijo.GetDirectories())
        {
          cont++;
          
          DataRow rowhijo = dtDirectorios.NewRow();
          rowhijo["K_Opcion"] = cont;
          rowhijo["K_Opcion_Padre"] = contPadre;
          rowhijo["D_Opcion"] = dh.Name;
          rowhijo["Enlace"] = "#" + d.Name + "#" + dh.Name;
          rowhijo["Icon"] = "far fa-file-video";
          rowhijo["Oculto"] = false;
          
          if (dtCarpetasUsuario.Columns.Count > 0 && dtCarpetasUsuario.Select("D_Carpeta = '" + dh.Name + "'").Length == 0)
          {
            rowhijo["Oculto"] = true;
          }

          dtDirectorios.Rows.InsertAt(rowhijo, 0);
        }
      }

      DataRow[] padres = dtDirectorios.Select("K_Opcion_Padre = 0");
      string p;

      for (int i= 0; i < padres.Length ; i++)
      {
        p = padres[i]["K_Opcion"].ToString();
        if (dtDirectorios.Select("K_Opcion_Padre = '" + p + "' AND Oculto = false").Length == 0)
        {
          padres[i]["Oculto"] = true;
        }
      }



      return dtDirectorios;
    }

    [WebMethod]
    public static void insertLog(string enlace)
    {
      ClassSeguridad.IN_Log_VideoVisto(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), enlace);      
    }
  }
}