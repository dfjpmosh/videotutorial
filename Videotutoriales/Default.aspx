﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Videotutoriales.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <br />
  <div id="lista" class="row" style="word-wrap: break-word">
        
  </div>    
  
  <div class="modal fade" id="ModalVideo" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="ModalVideo" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content shadow-sm rounded-sm border-0">

        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">          
          
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>   
  
  <script type="text/javascript" src="/Default.js"></script>
</asp:Content>
  


