﻿'use strict';
$(() => {

  $('.modal').on('shown.bs.modal', function (event) {
    let $btn = $(event.relatedTarget), $action = $btn.data('accion'), $name = $btn.data('nombre'), $modal = $(this);

    $modal.find('.modal-title').text($name);
    
    var elemento =
      ` <div class="embed-responsive embed-responsive-16by9">
          <video width="100%" controls controlslist="nodownload" autoplay loop>
            <source src="${$action}">
          </video>            
        </div> `
    $modal.find('.modal-body').append(elemento);

    insertaLog($action);
    
  });

  $('.modal').on('hidden.bs.modal', () => {

    $('.modal').find('.modal-body').find('.embed-responsive').remove();

  });

  document.oncontextmenu = function () { return false }
  
});

function insertaLog(video) {
  $.ajax({
    url: `Default.aspx/insertLog`,
    type: 'POST',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({ enlace: video }),
    beforeSend: function () {
    },
    success: function (data) {
      
    },
    error: function (error) {
      
    }
  });
}

$(document).ready(function () {
  let ruta = Cookies.get('ruta');

  if (ruta !== null && ruta !== undefined) {
    $.ajax({
      url: 'Default.aspx/ListaVideos2',
      type: "POST",
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify({ carpeta: ruta }),
      beforeSend: function () {
        loaderVencedor('#form1', 'show');
      },
      success: function (data) {
        if (data !== '') {
          var lista = document.getElementById("lista");
          let jData = JSON.parse(data.d);

          while (lista.firstChild) {
            lista.removeChild(lista.firstChild);
          }

          for (let val of jData) {
            var elemento =
              ` <div id="tarjeta" class="card col-12 col-md-4 col-lg-3">
                <div class="card bg-dark text-white" data-toggle="modal" data-target="#ModalVideo" data-accion="${val.Link}" data-nombre="${val.Nombre}">
                  <video width="100%" controlslist="nodownload" autoplay loop>
                    <source src="${val.Link}">
                  </video>
                  <div class="card-img-overlay" align="center">
                    <h5 class="card-title btn text-success font-weight-bold">${val.Nombre}</h5>
                  </div>
                </div>
              </div> `

            lista.insertAdjacentHTML("afterbegin", elemento);

            loaderVencedor('#form1', 'hide');
          }
        }
      },
      error: function (error) {
        loaderVencedor('#form1', 'hide');
        Toast.fire({
          icon: 'error',
          title: error.responseJSON.Message
        })
      }
    });
  }
});