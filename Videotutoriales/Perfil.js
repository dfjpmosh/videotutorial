﻿$(document).ready(function () {

  if (Cookies.get('D_Tipo_Usuario') !== "Administrador") {
    $("#cardNuevoUsuario").hide();
  }

  tbDinamic('#tbUsuarios', typeTable.tbUsuarios.onlyshow);
  slcTipoUsuario('#slcTipoUsuario');  

  $('.modal').on('shown.bs.modal', function (event) {
    let $btn = $(event.relatedTarget), $action = $btn.data('accion'), $name = $btn.data('nombre'), $modal = $(this);

    fnCargaChecks($modal);

  });

  $('.modal').on('hidden.bs.modal', () => {
    
  });  

  resetValues();  
});  

function fnAcciones(selector) {
  Alert.fire({
    type: 'warning',
    title: '¡Espera!',
    text: `¿Estas seguro de actualizar tu perfil?`,
    showCancelButton: true
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: 'Perfil.aspx/AccionesPerfil',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
          K_Usuario: Cookies.get('K_Usuario'),
          Contrasenia: $('#pswContraseña').val()
        }),
        success: function (data) {
          let jData = JSON.parse(data.d); 

          Toast.fire({
            type: 'success',
            title: `Contraseña de ${$('#txtNombreUsuario').val()} actualizada exitosamente.`
          });

          //WaitMe_Hide($(selector).parent());

          resetValues();
          //cambiarAcceso();
        },
        error: function (error) {
          Toast.fire({
            icon: 'error',
            title: error.responseJSON.Message
          })
        }
      });     
    }
  });
}

function resetValues() {
  $('#txtNombreUsuario').val(Cookies.get('D_Usuario'));
  $('#eCorreo').val(Cookies.get('Email'));
  $('#pswContraseña').val('');
  $('#pswConfirmarContraseña').val('');
  $('#actualizarContraseña').prop('checked', false);
}

$("#btnActualizarPerfil").on('click', function (e) {
  $('#form1').validate().destroy();
  let $formCreateRoute = $('#form1').validate({
    rules: {
      'txtNombreUsuario': {
        required: true,
        minlength: 5,
        maxlength: 30
      },
      'eCorreo': {
        required: true,
        email: true,
        maxlength: 50
      },
      'pswContraseña': {
        required: true
      },
      'pswConfirmarContraseña': {
        required: true,
        equalTo: {
          param: "#pswContraseña"          
        }
      }
    },
    messages: {
      'txtNombreUsuario': {
        required: "Este campo es requerido.",
        minlength: "Favor de ingresar un valor mayor o igual a 5 caracteres.",
        maxlength: "Favor de ingresar un valor menor a 30 caracteres."
      },
      'eCorreo': {
        required: "Este campo es requerido.",
        email: "Favor de ingresar una dirección de correo electrónico válida.",
        maxlength: "Favor de ingresar un valor menor a 50 caracteres."
      },
      'pswContraseña': {
        required: "Este campo es requerido."
      },
      'pswConfirmarContraseña': {
        required: "Este campo es requerido.",
        equalTo: "La contraseña no coinside"
      }
    },
    errorPlacement: function (error, element) {
      let container = $('<div></div>');
      container.addClass('invalid-feedback');
      error.insertAfter(element);
      error.wrap(container);
    },
    debug: false,
    errorElement: "label",
    errorClass: 'is-invalid',
    validClass: 'is-valid',
    submitHandler: function (form) {
      fnAcciones($('.modal').find('.modal-body'));
    }
  });
});

$("#btnCreaNuevoUsuario").on('click', function (e) {
  $('#form1').validate().destroy();
  let $formCreateRoute = $('#form1').validate({
    rules: {
      "txtNoNominaEmpleado": {
        required: true,
        number: true
      },
      "txtNombreNuevoUsuario": {
        required: true
      },
      "eCorreoNuevoUsuario": {
        required: true
      },
      "slcTipoUsuario": {
        required: true
      }
    },
    messages: {
      "txtNoNominaEmpleado": {
        required: "Este campo es requerido."
      },
      "txtNombreNuevoUsuario": {
        required: "Este campo es requerido."
      },
      "eCorreoNuevoUsuario": {
        required: "Este campo es requerido."
      },
      "slcTipoUsuario": {
        required: "Este campo es requerido."
      }
    },
    errorPlacement: function (error, element) {
      let container = $('<div></div>');
      container.addClass('invalid-feedback');
      error.insertAfter(element);
      error.wrap(container);
    },
    debug: false,
    errorElement: "label",
    errorClass: 'is-invalid',
    validClass: 'is-valid'
  });

  if ($formCreateRoute.form()) {
    $formCreateRoute.resetForm();
    fnCrearUsuario();
  }  
});

function fnCrearUsuario() {

  Alert.fire({
    type: 'warning',
    title: '¡Espera!',
    text: `¿Estas seguro de crear nuevo usuario?`,
    showCancelButton: true
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: 'Perfil.aspx/NuevoUsuario',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
          NoNomina: $("#txtNoNominaEmpleado").val(),
          Login: $("#txtNombreNuevoUsuario").val(),
          Email: $("#eCorreoNuevoUsuario").val(),
          TipoUsuario: $("#slcTipoUsuario").val(),
          Carpetas: fnPermisosCarpetas()
        }),
        beforeSend: function () {
          loaderVencedor('#ModalNuevoUsuario', 'show');
        },
        success: function (data) {
          let jData = JSON.parse(data.d);

          Toast.fire({
            type: 'success',
            title: `Usuario ${$("#txtNombreNuevoUsuario").val()} creado exitosamente.`
          });

          loaderVencedor('#ModalNuevoUsuario', 'hide');
          $('.modal').modal('hide');
          tbDinamic('#tbUsuarios', typeTable.tbUsuarios.onlyshow);
        },
        error: function (error) {
          loaderVencedor('#ModalNuevoUsuario', 'hide');
          Toast.fire({
            icon: 'error',
            title: error.responseJSON.Message
          })
        }
      });
    }
  });
}

function fnCargaChecks(modal) {
  let datos = filaSeleccionada('#tbUsuarios');

  $.ajax({
    url: '../Default.aspx/Menu',
    type: "POST",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({ K_Usuario: datos.K_Usuario }),
    success: function (data) {
      if (data !== '') {
        let jData = JSON.parse(data.d);

        $('#Usuario').text(datos.Usuario);
        $('#Usuario').val(datos.K_Usuario);

        modal.find('#Permisos').children().remove();

        for (let val of jData) {
          if (val.K_Opcion_Padre !== "0") {
            var check = val.Oculto === "False" ? "checked" : "";
            var elemento =
              ` <div class="col-12 col-md-4 col-lg-3 col-xl-2 mb-3">
                <input type="checkbox" class="custom-control-input justify-content-end chk_carpeta" id="${val.D_Opcion.replace(' ', '_')}" ${check} />
                <label class="custom-control-label font-italic" for="${val.D_Opcion.replace(' ', '_')}">${val.D_Opcion}</label>
              </div> `

            modal.find('#Permisos').append(elemento);
          }
        }        
      }
    },
    error: function (error) {
      Toast.fire({
        icon: 'error',
        title: error.responseJSON.Message
      })
    }
  });   
}

function fnPermisosCarpetas() {
  var chkCarpetas = document.getElementsByClassName("chk_carpeta");
  var arrCarpetas = [];

  for (i = 0; i < chkCarpetas.length; i++) {
    if (chkCarpetas[i].checked) {
      arrCarpetas.push(chkCarpetas[i].id)
    }    
  }

  return arrCarpetas;
}

function slcTipoUsuario(selector) {
  $.ajax({
    url: 'Perfil.aspx/TiposUsuarios',
    type: "POST",
    contentType: "application/json; charset=utf-8",
    data: null,
    success: function (data) {
      if (data !== '') {
        let jData = JSON.parse(data.d);
        $(selector).find('option').remove();
        for (let val of jData) {
          $(selector).append(`<option value='${val.K_Tipo_Usuario}'>${val.D_Tipo_USuario}</option>`);
        }

        $(selector).selectpicker('refresh');
        $(selector).trigger('change');

      } else {
        $(selector).append(`<option value=''></option>`);
      }
    },
    error: function (error) {
      Toast.fire({
        icon: 'error',
        title: error.responseJSON.Message
      })
    }
  });
}

$("#btnCreaPermisosUsuario").on('click', function (e) {
  Alert.fire({
    type: 'warning',
    title: '¡Espera!',
    text: `¿Estas seguro de Editar los Permisos?`,
    showCancelButton: true
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: 'Perfil.aspx/PermisosUsuario',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
          K_Usuario: $('#Usuario').val(),
          Carpetas: fnPermisosCarpetas()
        }),
        beforeSend: function () {
          loaderVencedor('#ModalNuevoUsuario', 'show');
        },
        success: function (data) {
          let jData = JSON.parse(data.d);

          loaderVencedor('#ModalNuevoUsuario', 'hide');
          Toast.fire({
            type: 'success',
            title: `Permisos editados exitosamente.`
          });

          $('.modal').modal('hide');
        },
        error: function (error) {
          loaderVencedor('#ModalNuevoUsuario', 'hide');
          Toast.fire({
            icon: 'error',
            title: error.responseJSON.Message
          })
        }
      });
    }
  });

});