﻿'use strict';

$(() => {
  $('#form1').validate({
    rules: {
      txtCorreo: {
        required: true
      },
      pswContraseña: {
        required: true
      }
    },
    messages: {
      txtCorreo: {
        required: 'Este campo es requerido.'
      },
      pswContraseña: {
        required: 'Este campo es requerido.'
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element).removeClass(validClass).addClass(errorClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass(errorClass).addClass(validClass);
    },
    success: function (label, errorClass, validClass) {
    },
    errorPlacement: function (error, element) {
      let container = $('<div></div>');
      container.addClass('invalid-feedback');
      error.insertAfter(element);
      error.wrap(container);
    },
    debug: false,
    errorElement: "label",
    errorClass: 'is-invalid',
    validClass: 'is-valid',
    submitHandler: function (form) {
      fnLogin();
    }
  });
});

function fnLogin() {
  $.ajax({
    url: `Login.aspx/login`,
    type: 'POST',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({ strCorreo: $('#txtCorreo').val(), strContrasenia: $('#pswContraseña').val(), strDirVideos: 'sivyl' }),
    beforeSend: function () {
      loaderVencedor('#form1', 'show');
    },
    success: function (data) {
      let JData = JSON.parse(data.d);

      loaderVencedor('#form1', 'hide');
      Toast.fire({
        type: 'success',
        title: JData.Message
      });
      window.location.href = '/Default';
    },
    error: function (error) {
      Toast.fire({
        icon: 'error',
        title: error.responseJSON.Message
      })
    }
  });
}


