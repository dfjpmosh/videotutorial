﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Videotutoriales.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Video Tutorial</title>

  <asp:PlaceHolder runat="server">
    <%: Scripts.Render("~/bundles/modernizr") %>
  </asp:PlaceHolder>

  <asp:PlaceHolder runat="server">
    <%: Styles.Render("~/bundles/login") %>
    <%: Styles.Render("~/bundles/waitalerts") %>
  </asp:PlaceHolder>

  <link href="~/grupo-vencedor-ico.png" rel="shortcut icon" type="image/x-icon" />
  <link type="text/css" href="/Content/bootstrap.css" rel="stylesheet"/>
  <link type="text/css" href="/Content/fontawesome-free-5.12.0-web/css/all.min.css" rel="stylesheet"/>
  <link type="text/css" href="/Scripts/plugins/loaderVencedor/loaderVencedor.css" rel="stylesheet"/>
  <link type="text/css" href="/Content/Login.css" rel="stylesheet"/>
  <link type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet'/>
</head>
<body class="text-center bg-grey-vencedor">
    <form id="form1" runat="server" class="form-signin m-auto">
      <asp:ScriptManager runat="server" EnablePageMethods="True" />
        <div class="needs-validation bg-white p-3 rounded-lg">
          <img src="/Images/GpoVencedor_Isotipo.png" class="logo-login mb-1 bg-grey-vencedor rounded-circle position-absolute" alt="..." />
          <h1 class="h4 my-4 font-weight-normal">Videotutoriales Grupo Vencedor</h1>
          <h2 id="titleSesion" class="h5 mb-3 font-weight-normal">Iniciar Sesión</h2>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="spCorreo"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" id="txtCorreo" name="txtCorreo" class="form-control" placeholder="Ingrese el correo" aria-describedby="spCorreo" />
          </div>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="spPassword"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" id="pswContraseña" name="pswContraseña" class="form-control" placeholder="Ingrese la contraseña" aria-describedby="spPassword" />
          </div>
          <div id="" class="input-group mb-3 d-none">
            <div class="input-group-prepend">
              <span class="input-group-text" id="spPasswordConfirm"><i class="fas fa-key"></i></span>
            </div>
            <input type="password" id="pswConfirmarContraseña" name="pswConfirmarContraseña" class="form-control" placeholder="Confirmar la contraseña" aria-describedby="spPasswordConfirm" />
          </div>
          <button id="btnSesion" type="submit" class="btn btn-primary rounded-0 mt-1"><i class='fas fa-sign-in-alt'></i>&nbsp Entrar</button>
          <p class="mt-5 text-muted text-center">&copy; <%: DateTime.Now.Year %></p>
        </div>
    </form>  
  <script type="text/javascript" src="/Scripts/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/popper.min.js"></script>
  <script type="text/javascript" src="/Scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/jquery-validation-1.19.0/dist/jquery.validate.min.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/jquery-validation-1.19.0/dist/additional-methods.min.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/sweetalert2-9.5.3/dist/sweetalert2.all.min.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/loaderVencedor/loaderVencedor.js"></script>
  <script type="text/javascript" src="/Scripts/plugins/waitMe-1.19/js/waitMe.min.js"></script>
  <script type="text/javascript" src="/Scripts/elements/preloaders&alerts.js"></script>  
  <script type="text/javascript" src="Login.js"></script>
</body>
</html>
