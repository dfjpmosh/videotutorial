﻿using Videotutoriales.App_Code;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Videotutoriales.Seguridad
{
  public partial class Perfil : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string AccionesPerfil(int K_Usuario, string Contrasenia)
    {
      string strRes;
      DataTable dt = new DataTable();
      dt.Columns.Add("K_Usuario");
      
      try
      {
        DataRow row = dt.NewRow();
        row["K_Usuario"] = K_Usuario;

        ClassSeguridad.UP_Usuarios_Pass(K_Usuario, Contrasenia);

        dt.Rows.Add(row);

        strRes = JsonConvert.SerializeObject(dt);
      }
      catch (Exception excep)
      {
        throw excep;
      }
      return strRes;
    }

    [WebMethod]
    public static void NuevoUsuario(string NoNomina, string Login, string Email, int TipoUsuario)
    {
      try
      {
        ClassSeguridad.IN_Usuarios(Login, Email, NoNomina, TipoUsuario);        
      }
      catch (Exception excep)
      {
        throw excep;
      }      
    }

    [WebMethod]
    public static void PermisosUsuario(int K_Usuario, string[] Carpetas)
    {
      DataTable dt = new DataTable();
      dt.Columns.Add("D_Carpeta");
      try
      {
        foreach (string carpeta in Carpetas)
        {
          DataRow row = dt.NewRow();
          row["D_Carpeta"] = carpeta.Replace("_", " ");
          dt.Rows.Add(row);
        }
        ClassSeguridad.INUP_Permisos_Usuarios(K_Usuario, dt);
      }
      catch (Exception excep)
      {
        throw excep;
      }
    }

    [WebMethod]
    public static string GP_Usuarios()
    {
      string strRes;
      DataTable dt;

      try
      {
        dt = ClassSeguridad.GP_Usuarios();

        strRes = JsonConvert.SerializeObject(dt);
      }
      catch (Exception excep)
      {
        throw excep;
      }
      return strRes;
    }

    [WebMethod]
    public static string TiposUsuarios()
    {
      string strRes;
      DataTable dt;

      try
      {
        dt = ClassSeguridad.GP_Tipos_Usuarios();

        strRes = JsonConvert.SerializeObject(dt);
      }
      catch (Exception excep)
      {
        throw excep;
      }
      return strRes;
    }

  }
}