﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="Videotutoriales.Seguridad.Perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <div class="row">
  <div class="col-12">
    <div class="card shadow-sm border-0">
      <div class="card-header bg-white">
        <div class="d-flex align-items-center">
          <strong class="flex-grow-1"><%: Title %></strong>
        </div>
      </div>
      <div class="card-body card-block">
        <div class="row">
          <div class="col-12 mb-3 mb-lg-0">
            <div class="card">
              <div class="card-header bg-transparent">
                <div class="d-flex align-items-center"> 
                  <strong class="flex-grow-1">Mi Perfil</strong>
                </div>                  
              </div>
              <div class="card-body bg-transparent">
                <div class="row">
                  <div class="col-12 col-md-6 col-lg-12 col-xl-6 mb-3">
                    <label for="txtNombreUsuario">Nombre de usuario:</label>
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span id="spNombreUsuario" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                          <i class="fas fa-user-circle text-dark"></i>
                        </span>
                      </div>
                      <input type="text" id="txtNombreUsuario" name="txtNombreUsuario" class="form-control border-top-0 border-right-0 border-left-0 rounded-0 text-uppercase" placeholder="Ingrese el nombre de usuario" aria-describedby="spNombreUsuario" autocomplete="off" disabled tabindex="1"/>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-12 col-xl-6 mb-3">
                    <label for="eCorreo">Correo electrónico:</label>
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span id="spCorreo" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                          <i class="fas fa-envelope text-dark"></i>
                        </span>
                      </div>
                      <input type="email" id="eCorreo" name="eCorreo" class="form-control border-top-0 border-right-0 border-left-0 rounded-0 text-lowercase" placeholder="usuario@correo.com" aria-describedby="spCorreo" disabled tabindex="2"/>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-12 col-xl-6 mb-3">
                    <label for="pswContraseña">Nueva contraseña:</label>
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span id="spContrasena" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                          <i class="fas fa-key text-dark"></i>
                        </span>
                      </div>
                      <input type="password" id="pswContraseña" name="pswContraseña" class="form-control border-top-0 border-right-0 border-left-0 rounded-0" aria-describedby="spContrasena" autocomplete="off" tabindex="3"/>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-12 col-xl-6 mb-3">
                    <label for="pswConfirmarContraseña">Confirmar contraseña:</label>
                    <div class="input-group input-group-sm">
                      <div class="input-group-prepend">
                        <span id="spConfirmarContraseña" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                          <i class="fas fa-key text-dark"></i>
                        </span>
                      </div>
                      <input type="password" id="pswConfirmarContraseña" name="pswConfirmarContraseña" class="form-control border-top-0 border-right-0 border-left-0 rounded-0" aria-describedby="spConfirmarContraseña" autocomplete="new-password" tabindex="4"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer bg-transparent d-flex justify-content-end align-items-center">
                <%--<div class="custom-control custom-checkbox mr-3">
                  <input type="checkbox" class="custom-control-input justify-content-end" id="actualizarContraseña" />
                  <label class="custom-control-label font-italic" for="actualizarContraseña">Actualizar contraseña</label>
                </div>--%>
                <button type="submit" id="btnActualizarPerfil" class="btn btn-success btn-sm rounded-sm shadow-sm"><i class="fas fa-user-edit mr-2" tabindex="5"></i>Actualizar Contraseña</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div id="cardNuevoUsuario" class="row">
    <div class="col-12">
      <div class="card shadow-sm border-0">
        <div class="card-header bg-white">
          <div class="d-flex align-items-center">
            <strong class="flex-grow-1"><%: Title %></strong>
          </div>
        </div>
        <div class="card-body card-block">
          <div class="row">
            <div class="col-12 mb-3 mb-lg-0">
              <div class="card">
                <div class="card-header bg-transparent">
                  <div class="d-flex align-items-center">
                    <strong class="flex-grow-1">Usuarios</strong>
                    <button type="button" id="btnEditarUsuario" class="btn btn-warning ml-2 btn-sm rounded-sm shadow-sm" data-toggle="modal" data-target="#ModalPermisosUsuario" data-accion="Editar" disabled><i class="fas fa-user-edit mr-2" tabindex="6"></i>Editar</button>
                    <button type="button" id="btnCreaUsuario" class="btn btn-primary ml-2 btn-sm rounded-sm shadow-sm" data-toggle="modal" data-target="#ModalNuevoUsuario" data-accion="Crear"><i class="fas fa-user-plus mr-2" tabindex="6"></i>Crear</button>
                  </div>
                </div>
                <div class="card-body bg-transparent">
                  <div class="row">
                    <div class="col-12">
                      <table id="tbUsuarios" style="width: 100%">
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ModalNuevoUsuario" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="ModalNuevoUsuario" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content shadow-sm rounded-sm border-0">

        <div class="modal-header">
          <h5 class="modal-title">Nuevo Usuario</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-6 col-xl-3 mb-3">
              <label for="txtNoNominaEmpleado">Número Nómina Empleado:</label>
              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span id="spNoNominaEmpleado" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                    <i class="fas fa-fingerprint text-dark"></i>
                  </span>
                </div>
                <input type="text" id="txtNoNominaEmpleado" name="txtNoNominaEmpleado" class="form-control border-top-0 border-right-0 border-left-0 rounded-0" placeholder="No. Nomina Empleado" aria-describedby="spNoNominaEmpleado" autocomplete="off" tabindex="1" />
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6 col-xl-3 mb-3">
              <label for="txtNombreNuevoUsuario">Nombre de usuario:</label>
              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span id="spNombreNuevoUsuario" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                    <i class="fas fa-user-circle text-dark"></i>
                  </span>
                </div>
                <input type="text" id="txtNombreNuevoUsuario" name="txtNombreNuevoUsuario" class="form-control border-top-0 border-right-0 border-left-0 rounded-0" placeholder="Ingrese el nombre de usuario" aria-describedby="spNombreNuevoUsuario" autocomplete="off" tabindex="2" />
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6 col-xl-3 mb-3">
              <label for="eCorreoNuevoUsuario">Correo electrónico:</label>
              <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                  <span id="spCorreoNuevoUsuario" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                    <i class="fas fa-envelope text-dark"></i>
                  </span>
                </div>
                <input type="email" id="eCorreoNuevoUsuario" name="eCorreoNuevoUsuario" class="form-control border-top-0 border-right-0 border-left-0 rounded-0 text-lowercase" placeholder="usuario@correo.com" aria-describedby="spCorreoNuevoUsuario" tabindex="3" />
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6 col-xl-3 mb-3">
              <div class="form-group">
                <label id="lblTipoUsuario">Tipo Usuario:</label>
                <div class="input-group input-group-sm">
                  <div class="input-group-prepend">
                    <span id="spTipoUsuario" class="input-group-text border-top-0 border-right-0 border-left-0 rounded-0">
                      <i class="far fa-building text-dark"></i>
                    </span>
                  </div>
                  <select id="slcTipoUsuario" name="slcTipoUsuario" class="form-control border-top-0 border-right-0 border-left-0 rounded-0" aria-describedby="spTipoUsuario" tabindex="4"></select>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm rounded-sm shadow-sm" data-dismiss="modal">Cerrar</button>
          <button type="button" id="btnCreaNuevoUsuario" class="btn btn-primary btn-sm rounded-sm shadow-sm">Crear</button>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="ModalPermisosUsuario" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="ModalPermisosUsuario" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content shadow-sm rounded-sm border-0">

        <div class="modal-header">
          <h5 class="modal-title">Permisos Usuario</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
          <div class="card shadow-sm border-0">
            <div class="card-header bg-white">
              <div class="d-flex align-items-center">
                <strong class="flex-grow-1" id="Usuario"></strong>
              </div>
            </div>
            <div class="card-body card-block">
              <div class="custom-control custom-checkbox mr-3">
                <div id="Permisos" class="row">                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-danger btn-sm rounded-sm shadow-sm" data-dismiss="modal">Cerrar</button>
          <button type="button" id="btnCreaPermisosUsuario" class="btn btn-primary btn-sm rounded-sm shadow-sm">Editar</button>
        </div>

      </div>
    </div>
  </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="scripts" runat="server">
  <script type="text/javascript" src="../Perfil.js"></script>
</asp:Content>
