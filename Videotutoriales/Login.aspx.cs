﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using Newtonsoft.Json;
using Videotutoriales.App_Code;

namespace Videotutoriales
{
  public partial class Login : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string login(string strCorreo, string strContrasenia, string strDirVideos)
    {
      ClassSeguridad classSeguridad = new ClassSeguridad();
      string strRes = string.Empty;
      
      try
      {
        if (string.IsNullOrEmpty(strCorreo))
          throw new Exception("Falta ingresar el correo.");

        if (string.IsNullOrEmpty(strContrasenia))
          throw new Exception("Falta ingresar la contraseña.");

        DataTable dt = classSeguridad.Gp_Acceso_Usuario(strCorreo, strContrasenia);

        FormsAuthentication.Initialize();

        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, strCorreo, DateTime.Now, DateTime.Now.AddHours(8), false, FormsAuthentication.FormsCookiePath);
        string hash = FormsAuthentication.Encrypt(ticket);

        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

        HttpCookie cookieIdUsuario = new HttpCookie("K_Usuario", Convert.ToString(dt.Rows[0]["K_Usuario"]));
        HttpCookie cookieUsuario = new HttpCookie("D_Usuario", Convert.ToString(dt.Rows[0]["D_Usuario"]));
        HttpCookie cookieEmail = new HttpCookie("Email", Convert.ToString(dt.Rows[0]["Email"]));
        HttpCookie cookieIdRhClave = new HttpCookie("IdRhClave", Convert.ToString(dt.Rows[0]["IdRhClave"]));
        HttpCookie cookieRhClave = new HttpCookie("RhClave", Convert.ToString(dt.Rows[0]["RhClave"]));
        HttpCookie cookieRhNombre = new HttpCookie("RhNombre", Convert.ToString(dt.Rows[0]["RhNombre"]));
        HttpCookie cookieRhAPaterno = new HttpCookie("RhAPaterno", Convert.ToString(dt.Rows[0]["RhAPaterno"]));
        HttpCookie cookieRhAMaterno = new HttpCookie("RhAMaterno", Convert.ToString(dt.Rows[0]["RhAMaterno"]));        
        HttpCookie cookieRhEmpresa = new HttpCookie("RhEmpresa", Convert.ToString(dt.Rows[0]["RhEmpresa"]));
        HttpCookie cookieRhDepartamento = new HttpCookie("RhDepartamento", Convert.ToString(dt.Rows[0]["RhDepartamento"]));
        HttpCookie cookieRhPuesto = new HttpCookie("RhPuesto", Convert.ToString(dt.Rows[0]["RhPuesto"]));
        HttpCookie cookieTipoUsuario = new HttpCookie("D_Tipo_Usuario", Convert.ToString(dt.Rows[0]["D_Tipo_USuario"]));

        HttpCookie cookieDirVideos = new HttpCookie("DirVideos", strDirVideos);

        HttpContext.Current.Response.Cookies.Add(cookie);

        HttpContext.Current.Response.Cookies.Add(cookieIdUsuario);
        HttpContext.Current.Response.Cookies.Add(cookieUsuario);
        HttpContext.Current.Response.Cookies.Add(cookieEmail);
        HttpContext.Current.Response.Cookies.Add(cookieIdRhClave);
        HttpContext.Current.Response.Cookies.Add(cookieRhClave);
        HttpContext.Current.Response.Cookies.Add(cookieRhNombre);
        HttpContext.Current.Response.Cookies.Add(cookieRhAPaterno);
        HttpContext.Current.Response.Cookies.Add(cookieRhAMaterno);
        HttpContext.Current.Response.Cookies.Add(cookieRhEmpresa);
        HttpContext.Current.Response.Cookies.Add(cookieRhDepartamento);
        HttpContext.Current.Response.Cookies.Add(cookieRhPuesto);
        HttpContext.Current.Response.Cookies.Add(cookieTipoUsuario);

        HttpContext.Current.Response.Cookies.Add(cookieDirVideos);

        //HttpContext.Current.Response.Redirect("/Default");

        strRes = JsonConvert.SerializeObject(new { Message = "Acceso correcto." });
      }
      catch (Exception excep)
      {
        throw excep;
        //ClassSeguridad.log_errors(0, string.Empty, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
      }

      return strRes;
    }

    [WebMethod]
    public static string Carpeta()
    {
      DataTable dtDir = new DataTable();
      dtDir.Columns.Add("Nombre");

      System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath(ClassConection.getDirectorioPrincipal()));
      foreach (System.IO.DirectoryInfo d in dir.GetDirectories())
      {
        DataRow row = dtDir.NewRow();
        row["Nombre"] = d.Name;

        dtDir.Rows.InsertAt(row, 0);
      }

      return JsonConvert.SerializeObject(dtDir);
    }

  }
}