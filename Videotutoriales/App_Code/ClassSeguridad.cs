﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Videotutoriales.App_Code
{
  public class ClassSeguridad : ClassConection
  {
    public DataTable Gp_Acceso_Usuario(string Email, string Contrasenia)
    {
      DataTable dt = new DataTable();
      using (var connection = GetConnection())
      {
        using (SqlConnection conn = GetConnection())
        {
          try
          {
            conn.Open();

            SqlCommand cmd = new SqlCommand("Gp_Acceso_Usuario", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.AddWithValue("@K_Usuario_Log", Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value));
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@Contrasenia", Contrasenia);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
              dt.Load(reader);
            }
          }
          catch (Exception exception)
          {
            //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
            throw exception;
          }
        }
      }
      return dt;
    }

    public static int IN_Usuarios(string D_Usuario, string Email, string IdClave, int TipoUsuario)
    {
      using (SqlConnection conn = GetConnection())
      {
        try
        {
          conn.Open();

          SqlCommand cmd = new SqlCommand("IN_Usuarios", conn);
          cmd.CommandType = CommandType.StoredProcedure;

          SqlParameter p_Usuario = cmd.Parameters.Add("@K_Usuario", SqlDbType.Int);
          p_Usuario.Direction = ParameterDirection.InputOutput;

          cmd.Parameters.AddWithValue("@D_Usuario", D_Usuario);
          cmd.Parameters.AddWithValue("@Email", Email);
          cmd.Parameters.AddWithValue("@IdClave", IdClave);
          cmd.Parameters.AddWithValue("@K_Tipo_Usuario", TipoUsuario);

          cmd.ExecuteNonQuery();

          return (int)p_Usuario.Value;
        }
        catch (Exception exception)
        {
          //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
          throw exception;
        }
      }
    }

    public static void INUP_Permisos_Usuarios(int K_Usuario, DataTable Carpetas)
    {
      using (SqlConnection conn = GetConnection())
      {
        try
        {
          conn.Open();

          SqlCommand cmd = new SqlCommand("INUP_Permisos_Usuarios", conn);
          cmd.CommandType = CommandType.StoredProcedure;

          cmd.Parameters.AddWithValue("@K_Usuario", K_Usuario);
          
          SqlParameter tbl = new SqlParameter("@Carpetas", Carpetas);
          tbl.SqlDbType = SqlDbType.Structured;
          tbl.TypeName = "Carpetas";
          cmd.Parameters.Add(tbl);

          cmd.ExecuteNonQuery();
        }
        catch (Exception exception)
        {
          //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
          throw exception;
        }
      }
    }

    public static void UP_Usuarios_Pass(int K_Usuario, string Contrasena)
    {
      using (SqlConnection conn = GetConnection())
      {
        try
        {
          conn.Open();

          SqlCommand cmd = new SqlCommand("UP_Usuarios_Pass", conn);
          cmd.CommandType = CommandType.StoredProcedure;

          cmd.Parameters.AddWithValue("@K_Usuario", K_Usuario);
          cmd.Parameters.AddWithValue("@Contrasena", Contrasena);

          cmd.ExecuteNonQuery();
        }
        catch (Exception exception)
        {
          //log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
          throw exception;
        }
      }
    }

    public static DataTable GP_Usuarios()
    {
      DataTable dt = new DataTable();
      using (var connection = GetConnection())
      {
        using (SqlConnection conn = GetConnection())
        {
          try
          {
            conn.Open();

            SqlCommand cmd = new SqlCommand("Gp_Usuarios", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
              dt.Load(reader);
            }
          }
          catch (Exception exception)
          {
            //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
            throw exception;
          }
        }
      }
      return dt;
    }

    public static DataTable GP_Tipos_Usuarios()
    {
      DataTable dt = new DataTable();
      using (var connection = GetConnection())
      {
        using (SqlConnection conn = GetConnection())
        {
          try
          {
            conn.Open();

            SqlCommand cmd = new SqlCommand("Gp_Tipos_Usuarios", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
              dt.Load(reader);
            }
          }
          catch (Exception exception)
          {
            //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
            throw exception;
          }
        }
      }
      return dt;
    }

    public static DataTable SK_Carpetas_Usuarios(int K_Usuario)
    {
      DataTable dt = new DataTable();
      using (var connection = GetConnection())
      {
        using (SqlConnection conn = GetConnection())
        {
          try
          {
            conn.Open();

            SqlCommand cmd = new SqlCommand("SK_Carpetas_Usuarios", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@K_Usuario", K_Usuario);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
              dt.Load(reader);
            }
          }
          catch (Exception exception)
          {
            //ClassSeguridad.log_errors(Convert.ToInt32(HttpContext.Current.Request.Cookies["K_Usuario"].Value), HttpContext.Current.Request.Cookies["D_Empleado"].Value, exception.Message, exception.Source, exception.TargetSite.DeclaringType.Name, exception.TargetSite.Name, exception.StackTrace, ClassSeguridad.typeexcep.Exception);
            throw exception;
          }
        }
      }
      return dt;
    }
  }
}