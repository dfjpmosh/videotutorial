﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Videotutoriales.App_Code
{
  public class ClassConection
  {
    public static string getConnectionString()
    {
      string connectionString = string.Empty;
      connectionString = ConfigurationManager.ConnectionStrings["Videotutorial_Production"].ConnectionString;

      return connectionString;
    }

    protected static SqlConnection GetConnection()
    {
      return new SqlConnection(getConnectionString());
    }

    public static string getDirectorioPrincipal()
    {
      return "Videos";
    }

    public static void IN_Log_VideoVisto(int K_Usuario, string enlace)
    {
      using (SqlConnection conn = GetConnection())
      {
        try
        {
          conn.Open();

          SqlCommand cmd = new SqlCommand("IN_Log_VideoVisto", conn);
          cmd.CommandType = CommandType.StoredProcedure;

          cmd.Parameters.AddWithValue("@K_Usuario", K_Usuario);
          cmd.Parameters.AddWithValue("@Enlace", enlace);

          cmd.ExecuteNonQuery();
        }
        catch (Exception exception)
        {
          throw exception;
        }
      }
    }
  }
}