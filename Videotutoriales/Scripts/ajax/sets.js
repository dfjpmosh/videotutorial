﻿'use strict';

function setTiposUsuarios(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'TypeUsers.aspx/setTypeUsers',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}

function setUsuarios(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'Users.aspx/setUsers',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}

function setGrupos(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'Groups.aspx/setGroups',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}