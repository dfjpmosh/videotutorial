﻿'use strict';

function getDatas(jParams, fnBeforeSend, fnSuccess, fnError, url) {
  $.ajax({
    url: url,
    type: 'POST',
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify(jParams),
    beforeSend: fnBeforeSend,
    success: fnSuccess,
    error: fnError
  });
}