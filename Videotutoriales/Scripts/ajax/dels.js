﻿'use strict';

function delTiposUsuarios(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'TypeUsers.aspx/delTypeUsers',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}

function delUsuarios(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'Users.aspx/delUsers',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}

function delGrupos(jParams, fnBeforeSend, fnSuccess, fnError) {
    $.ajax({
        url: 'Groups.aspx/delGroups',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(jParams),
        beforeSend: fnBeforeSend,
        success: fnSuccess,
        error: fnError
    });
}