﻿'use strict';

const typeHeaders = {
  tbUsuarios: {
    Usuario: {
      headName: 'Usuario',
      rowSpan: 1,
      colSpan: 1
    },
    Email: {
      headName: 'Email',
      rowSpan: 1,
      colSpan: 1
    },
    NoNomina: {
      headName: 'NoNomina',
      rowSpan: 1,
      colSpan: 1
    },
    TipoUsuario: {
      headName: 'TipoUsuario',
      rowSpan: 1,
      colSpan: 1
    },
    Estado: {
      headName: 'Estado',
      rowSpan: 1,
      colSpan: 1
    }
  }
};