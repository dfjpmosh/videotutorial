﻿'use strict';

function tbDinamic($selector, typeTable, JParams = {}, B_Reporte = false, B_Multi_Single = false) {
  let $table = $($selector);
  $table.addClass('table table-hover table-striped table-bordered table-sm order-column display nowrap'); 

  var $parent_div = $table.parents('.card');

  let fnBeforeSend = () => {
    loaderVencedor($parent_div, 'show');
  };

  let fnSuccess = (data) => {
    try {
      createHeads($table, typeTable.headers);
      createFooters($table, typeTable.footers);

      if ($.fn.DataTable.isDataTable($selector)) {
        $table.DataTable().clear().destroy();
      }

      let jData = data.d !== '' ? JSON.parse(data.d) : [];

      let jsonOptions = $.extend({}, typeTable);
      jsonOptions.data = jData;

      addDefaultOptions(jsonOptions, `Usuarios ${moment().format('DD-MM-YYYY')}`, $selector, B_Reporte, B_Multi_Single);
      isInit(jsonOptions);

      $($selector).DataTable(jsonOptions);

      Toast.fire({
        type: 'success',
        title: 'Datos cargados correctamente.'
      });

      $table.DataTable().draw(false);
    } catch (e) {
      console.error(e);
    }
    loaderVencedor($parent_div, 'hide');
  };

  let fnError = (error) => {
    Toast.fire({
      type: 'warning',
      title: error.responseJSON.Message
    });
    console.log(error);
    loaderVencedor($parent_div, 'hide');
  };

  getDatas(
    JParams,
    fnBeforeSend,
    fnSuccess,
    fnError,
    typeTable.url
  );
}

function tbTemporal($selector, typeTable) {
  let $table = $($selector);

  try {
    createHeads($table, typeTable.headers);
    createFooters($table, typeTable.footers);

    let jsonOptions = $.extend({}, typeTable);

    //addDefaultOptions(jsonOptions, `${Cookies.get('D_Opcion')} ${moment().format('DD-MM-YYYY')}`, $selector, false, true);

    $($selector).DataTable(jsonOptions);
  } catch (e) {
    console.error(e);
  }
}

function filaSeleccionada(selector) {
  let table = $(selector).DataTable();
  let datas = table.row($(`${selector} tbody tr.selected`)).data();

  return datas;
}

