﻿"use strict";

const typeColumns = {
  tbUsuarios: {
    Usuario: {
      data: 'Usuario',
      className: 'text-center'
    },
    Email: {
      data: 'Email',
      className: 'text-center'
    },
    NoNomina: {
      data: 'NoNomina',
      className: 'text-center'
    },
    TipoUsuario: {
      data: 'TipoUsuario',
      className: 'text-center'
    },
    Estado: {
      data: 'Estado',
      className: 'text-center'
    }
  }
};