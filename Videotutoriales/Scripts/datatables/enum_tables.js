﻿ 'use strict';

const typeTable = {
  tbUsuarios: {
    onlyshow: {
      rowId: 'K_Usuario',
      headers: [
        [
          typeHeaders.tbUsuarios.Usuario,
          typeHeaders.tbUsuarios.Email,
          typeHeaders.tbUsuarios.NoNomina,
          typeHeaders.tbUsuarios.TipoUsuario,
          typeHeaders.tbUsuarios.Estado
        ]
      ],
      columns: [
        typeColumns.tbUsuarios.Usuario,
        typeColumns.tbUsuarios.Email,
        typeColumns.tbUsuarios.NoNomina,
        typeColumns.tbUsuarios.TipoUsuario,
        typeColumns.tbUsuarios.Estado

      ],
      footers: [
        [
          typeFooters.tbUsuarios.Usuario,
          typeFooters.tbUsuarios.Email,
          typeFooters.tbUsuarios.NoNomina,
          typeFooters.tbUsuarios.TipoUsuario,
          typeFooters.tbUsuarios.Estado
        ]
      ],
      url: '/Seguridad/Perfil.aspx/GP_Usuarios'
    }
  }
};