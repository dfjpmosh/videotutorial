﻿'use strict';

function getRow($table, el) {
    return $table.row(el).data();
}

function setRow($table, datas, idRow, type) {
    type === enumEditions.CREATE
        ? $table.row.add(datas).node().id = idRow
        : $table.row(`#${idRow}`).data(datas);
    $table.draw(false);
}

function delRow($table, idRow) {
    $table.row(`#${idRow}`).remove();
    $table.draw(false);
}