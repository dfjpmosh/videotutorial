﻿'use strict';

function addDefaultOptions(jsonOptions, headerTitle, cTable, B_Reporte, B_Multi_Single) {
  jsonOptions.language = {
    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"
  };
  jsonOptions.lengthMenu = [[100, 300, 500], [100, 300, 500]];
  jsonOptions.scrollY = '53vh';
  jsonOptions.scrollCollapse = true;
  jsonOptions.scrollX = true;
  jsonOptions.select = {
    style: B_Multi_Single ? 'multi' : 'single'
  };
  jsonOptions.dom = `<'row'<'col-12 col-sm-6 col-lg-4 col-xl-4 d-flex justify-content-center justify-content-md-start order-0'l><'col-12 col-lg-3 col-xl-4 d-flex justify-content-center order-2 order-lg-1'${B_Reporte ? 'B' : ''}><'col-12 col-sm-6 col-lg-5 col-xl-4 d-flex justify-content-center justify-content-md-end order-1 order-lg-2'f>>" +
        "<'row'<'col-12 'tr>>" +
        "<'row'<'col-12 col-md-5'i><'col-12 col-md-7'p>>`;
  jsonOptions.buttons = {
    dom: {
      button: {
        tag: 'button',
        className: ''
      }
    },
    buttons: [
      {
        extend: 'excelHtml5',
        text: '<i class="fas fa-file-excel"></i>',
        className: 'btn btn-success btn-sm shadow-sm rounded-sm ml-2',
        titleAttr: 'Exportar a Excel',
        filename: headerTitle,
        exportOptions: {
          columns: ':visible'
        },
        customize: function (xlsx) {
          var sheet = xlsx.xl.worksheets['sheet1.xml'];

          $('row c[r^="C"]', sheet).attr('s', '2');
        }
      },
      {
        extend: 'pdfHtml5',
        text: '<i class="fas fa-file-pdf"></i>',
        className: 'btn btn-danger btn-sm shadow-sm rounded-sm ml-2',
        titleAttr: 'Exportar a PDF',
        filename: headerTitle,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'colvis',
        text: '<i class="fas fa-columns"></i>',
        className: 'btn btn-info btn-sm shadow-sm rounded-sm ml-2',
        titleAttr: 'Ocultar columnas',
        filename: headerTitle,
        exportOptions: {
          columns: ':visible'
        }
      }
    ]
  };
}

function isInit(jsonOptions) {
  jsonOptions.initComplete = function (settings, json) {
    let $table = $(this[0]), $card = $table.parents('.card');
    if ($card.length !== 0) {
      let $btns = $card.children().first().find('button');
      $btns.each(function () {
        let $btn = $(this), $accion = $btn.data('accion');

        if ($accion !== 'Crear')
          $(this).prop('disabled', true);
      });

      $(this[0]).on('click', 'tr', function () {
        let $tr = $(this), $btns = $card.children().first().find('button');
        $btns.each(function () {
          let $btn = $(this), $accion = $btn.data('accion');

          if ($accion !== 'Crear')
            $btn.prop('disabled', $tr.hasClass('selected'));
        });
      });
    }

    this.api().columns().every(function () {
      let column = this, $select = $('<input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" />'),
        $div = $(`<div class="input-group input-group-sm"></div>`).appendTo($(column.footer()).empty());

      $($div).append($select);

      $($select).on('keyup change clear', function () {
        if (column.search() !== this.value) {
          column
            .search(this.value)
            .draw();
        }
      });

    });

    this.api().draw();
  };
}

function createHeads(table, arrHeaders) {
  table.html('');
  let $thead = $('<thead class="text-center" />');

  for (let itemTr of arrHeaders) {
    let $tr = $('<tr>');

    for (let itemTh of itemTr) {
      let rowSpan, colSpan, className;

      itemTh.rowSpan === undefined ? rowSpan = 1 : rowSpan = itemTh.rowSpan;
      itemTh.colSpan === undefined ? colSpan = 1 : colSpan = itemTh.colSpan;
      itemTh.className === undefined ? className = "" : className = itemTh.className;

      let $th = $(`<th rowspan='${rowSpan}' colspan='${colSpan}' class='${className}'>`);

      itemTh.headName ? $th.html(itemTh.headName) : '';

      $tr.append($th);
    }

    $thead.append($tr);

  }

  table.append($thead);
}

function createFooters(table, arrFooters) {
  let $tfoot = $('<tfoot class="text-center" />');

  for (let itemTr of arrFooters) {
    let $tr = $('<tr>');

    for (let itemTh of itemTr) {
      let rowSpan, colSpan, className;

      itemTh.rowSpan === undefined ? rowSpan = 1 : rowSpan = itemTh.rowSpan;
      itemTh.colSpan === undefined ? colSpan = 1 : colSpan = itemTh.colSpan;
      itemTh.className === undefined ? className = "" : className = itemTh.className;

      let $th = $(`<th rowspan='${rowSpan}' colspan='${colSpan}' class='${className}'>`);

      itemTh.headName ? $th.html(itemTh.headName) : '';

      $tr.append($th);
    }

    $tfoot.append($tr);

  }

  table.append($tfoot);
}