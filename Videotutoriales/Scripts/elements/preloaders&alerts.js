﻿const Toast = Swal.mixin({
  customClass: {
    popup: 'rounded-sm shadow'
  },
  toast: true,
  position: 'bottom',
  showConfirmButton: false,
  timer: 3000
});

const Alert = Swal.mixin({
  customClass: {
    container: 'container-class',
    popup: 'rounded-sm shadow',
    header: 'header-class',
    title: 'title-class',
    closeButton: 'close-button-class',
    image: 'image-class',
    content: 'content-class',
    input: 'input-class',
    actions: 'actions-class',
    confirmButton: 'btn btn-primary btn-sm rounded-sm mr-3  shadow-sm',
    cancelButton: 'btn btn-danger btn-sm rounded-sm shadow-sm',
    footer: 'footer-class'
  },
  buttonsStyling: false,
  allowOutsideClick: false,
  allowEscapeKey: false,
  allowEnterKey: false,
  reverseButtons: false,
  confirmButtonText: 'Sí<i class="fas fa-thumbs-up ml-2"></i>',
  cancelButtonText: 'No<i class="fas fa-thumbs-down ml-2"></i>'
});

function WaitMe_Show(idForm) {
  $(idForm).waitMe({
    effect: 'win8_linear',
    text: '',
    bg: 'rgb(255,255,255)',
    color: '#5082d3',
    maxSize: 30,
    waitTime: -1,
    textPos: 'vertical',
    fontSize: '',
    source: '',
    onClose: () => { }
    //sizeW: '',
    //sizeH: '',
  });
}

function WaitMe_Hide(idForm) {
  $(idForm).waitMe('hide');
}