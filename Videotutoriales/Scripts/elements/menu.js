﻿function accesoMenu(selector) {
  $.ajax({
    url: '../Default.aspx/Menu',
    type: "POST",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({ K_Usuario: Cookies.get('K_Usuario') }),
    success: function (data) {
      if (data !== '') {
        let jData = JSON.parse(data.d), menu_list = [];

        jData.forEach((f) => {
          f.K_Opcion = parseInt(f.K_Opcion);
          f.K_Opcion_Padre = parseInt(f.K_Opcion_Padre);
          
          if (f.Oculto === "False") {
            menu_list.push(f);
          }
        });

        $(selector).children().remove();
        showNameUser();
        crearMenuPadre(menu_list, selector);
        //navActions();
      } else {
        $(selector).remove();
      }
      //WaitMe_Hide($(selector));
    },
    error: function (error) {
      Toast.fire({
        icon: 'error',
        title: error.responseJSON.Message
      })
    }
  });   
}

function crearMenuPadre(menu, selector) {
  let flPadre = menu.filter(f => { return f.K_Opcion_Padre === 0; }), menuString = '';
  for (let val of flPadre) {
    crearSubMenu(menu, val, selector);
  }
}

function crearSubMenu(menu, flPadre, selector) {
  let flSubMenu = menu.filter(f => { return f.K_Opcion_Padre === flPadre.K_Opcion; }),
    $li = $(`<li class="nav-item" />`);

  if (flSubMenu.length > 0) {
    let D_Opcion = flPadre.D_Opcion, $id = D_Opcion.replace(/[^-A-Za-z0-9]+/g, '').toLowerCase(),
      $ul = $(`<ul class="nav" />`),
      $div = $(`<div id="${$id}" class="collapse" />`);
    $a = $(`<a href="#${$id}" class="nav-link mt-2 mx-3 mb-0 text-capitalization" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="${$id}" data-section="${flPadre.D_Opcion}" />`);

    $a.append(`<i class="${flPadre.Icon} mr-3"/>`).append(`<span>${flPadre.D_Opcion}</span>`).append(`<i class="fas fa-angle-down float-right pt-1" />`);
    $li.append($a);
    $div.append($ul);
    $li.append($div);

    for (let val of flSubMenu) {
      crearSubMenu(menu, val, $ul);
    }
  } else {
    //let $a = $(`<a href="${flPadre.Enlace}" class="nav-link mt-2 mx-3 mb-0" data-section="${flPadre.D_Opcion}" onClick="alert('¡Has hecho clic!')"/>`);
    let $a = $(`<a href="#" class="nav-link mt-2 mx-3 mb-0" data-section="${flPadre.D_Opcion}" onClick="Directorio('${flPadre.Enlace}')" />`);
    $a.append(`<i class="${flPadre.Icon} mr-3"/>`).append(`<span>${flPadre.D_Opcion}</span>`);
    $li.append($a);
  }
  $(selector).append($li);
}

function Directorio(ruta) {
  Cookies.set('ruta', ruta);
  location.href = "../Default"; 

  //$.ajax({
  //  url: 'Default.aspx/ListaVideos2',
  //  type: "POST",
  //  contentType: "application/json; charset=utf-8",
  //  data: JSON.stringify({ carpeta: ruta}),
  //  success: function (data) {
  //    if (data !== '') {
  //      var lista = document.getElementById("lista");
  //      let jData = JSON.parse(data.d);

  //      while (lista.firstChild) {
  //        lista.removeChild(lista.firstChild);
  //      }

  //      for (let val of jData) {
  //        var elemento =
  //          ` <div id="tarjeta" class="card col-12 col-md-4 col-lg-3">
  //              <div class="card bg-dark text-white" data-toggle="modal" data-target="#ModalVideo" data-accion="${val.Link}" data-nombre="${val.Nombre}">
  //                <video width="100%" controlslist="nodownload" autoplay loop>
  //                  <source src="${val.Link}">
  //                </video>
  //                <div class="card-img-overlay" align="center">
  //                  <h5 class="card-title btn text-success font-weight-bold">${val.Nombre}</h5>
  //                </div>
  //              </div>
  //            </div> `

  //        lista.insertAdjacentHTML("afterbegin", elemento);
  //      }
  //    }
  //  },
  //  error: function (error) {
  //    Toast.fire({
  //      icon: 'error',
  //      title: error.responseJSON.Message
  //    })
  //  }
  //}); 
}

function showNameUser() {
  let $pEmpresa = $(`<p>${Cookies.get('RhEmpresa')}</p>`),
    $pDepartamento = $(`<p>${Cookies.get('RhDepartamento')}</p>`),
    $pTipoUsuario = $(`<p>${Cookies.get('RhPuesto')}</p>`),
    $clpeOrganizacion = $('#clpeOrganizacion').html('');

  $('.user-name').html('').append(Cookies.get('D_Usuario'));

  $([$pEmpresa, $pDepartamento, $pTipoUsuario]).each(function () { $(this).addClass('mb-1').appendTo($clpeOrganizacion); });
}