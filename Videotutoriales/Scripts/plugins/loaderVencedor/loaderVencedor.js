function loaderVencedor($element, method) {
    try {
        if (method === 'show') {
            if ($element !== '') {
                $($element).addClass('loaderVencedor-container');

                $($element).append(`
                    <div class="loaderVencedor">
                        <div class="img-vencedor"></div>
                        <div class="rotate-one"></div>
                        <div class="rotate-tow"></div>
                    </div>
                `);
            } else {
                throw "Falta selector, un elemento donde mostrar u ocultar el loader Vencedor.";
            }
        } else if (method === 'hide') {
            $($element).removeClass('loaderVencedor-container');
            $($element)
                .children('.loaderVencedor')
                .remove();
        } else {
            throw 'Falta un m�todo show o hide.';
        }
    } catch (e) {
        console.error(e);
    }
}