﻿$(document).ready(function () {
  //$('.sidebar').mCustomScrollbar({
  //  theme: "minimal"
  //});

  $('#sidebarCollapse').on('click', function () {
    $('.sidebar').toggleClass('active');
    $('.main-panel').toggleClass('active');
    $(this).toggleClass('active');

    setTimeout(function () {
      $('table').DataTable().draw();
    }, 600);
  });
});